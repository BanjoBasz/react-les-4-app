export const CHANGE_SEARCHTERM = "CHANGE_SEARCHTERM";
export const CHANGE_VIDEO = "CHANGE_VIDEO";

export const changeSearchterm = searchTerm => ({
  type: CHANGE_SEARCHTERM,
  payload: searchTerm
});

export const changeVideo = video => ({
  type: CHANGE_VIDEO,
  payload: video
});
