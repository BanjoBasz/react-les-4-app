import { CHANGE_SEARCHTERM, CHANGE_VIDEO } from "./actions.js";

export const searchTerm = (state = "", action) => {
  switch (action.type) {
    case CHANGE_SEARCHTERM:
      return action.payload;
    default:
      return state;
  }
};

export const video = (state = "", action) => {
  switch (action.type) {
    case CHANGE_VIDEO:
      return "http://localhost:8000" + action.payload;
    default:
      return state;
  }
};
