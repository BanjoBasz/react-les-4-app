import React from "react";

import "./VideoPlayer.css";
import { connect } from "react-redux";
const VideoPlayer = props => {
  console.log("rerender?: " + props.video);
  return (
    <div>
      <video className="videoPlayer" key={props.video} width="400" controls>
        <source src={props.video || ""} type="video/mp4" controls />
      </video>
    </div>
  );
};

const mapStateToProps = state => {
  return { video: state.video };
};

export default connect(mapStateToProps)(VideoPlayer);
