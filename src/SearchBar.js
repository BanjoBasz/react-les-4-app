import "./SearchBar.css";
import React from "react";
import axios from "axios";
import { connect } from "react-redux";
import { changeSearchterm, changeVideo } from "./actions";

class SearchBar extends React.Component {
  componentDidMount() {
    this.makeApiCall(2019);
  }

  onSubmit = e => {
    e.preventDefault();
    this.makeApiCall(this.props.searchTerm);
  };

  makeApiCall = searchTerm => {
    const BASE_URL = "http://localhost:8000/api/winners/";
    axios.get(BASE_URL + searchTerm).then(res => {
      this.props.changeVideo(res.data.winner.video);
    });
  };

  onSearch = event => {
    this.props.changeSearchterm(event.target.value);
  };

  render() {
    console.log();
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            className="seachbar"
            type="text"
            placeholder="Zoek hier op jaartal"
            onChange={this.onSearch}
            value={this.props.searchTerm}
          />
        </form>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return { searchTerm: state.searchTerm };
};
export default connect(
  mapStateToProps,
  { changeSearchterm: changeSearchterm, changeVideo: changeVideo }
)(SearchBar);
