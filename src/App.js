import React from "react";
import SearchBar from "./SearchBar";
import VideoPlayer from "./VideoPlayer";
import "./App.css";
import { Provider } from "react-redux";
import { store } from "./store";

class App extends React.Component {
  render() {
    return (
      <div className="container">
        <h1>De reactieve eurovisie video player</h1>
        <Provider store={store}>
          <SearchBar onSubmit={this.makeApiCall} />
          <VideoPlayer />
        </Provider>
      </div>
    );
  }
}
export default App;
